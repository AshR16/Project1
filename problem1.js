function problem1(inventory, id) {
    if(inventory == undefined || id == undefined || inventory.length == 0 ){
        return [];
    }
    for (let i=0; i < inventory.length; i++){
        if (inventory[i].id === id){
            return `"Car ${id} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}"`
        }
    }
}

module.exports = problem1;

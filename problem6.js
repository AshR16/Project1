function problem6(inventory) {
    
    let audiAndBmwCars = [];
    for (let i=0; i < inventory.length; i++){
        if (inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi") {
            audiAndBmwCars.push(inventory[i])
        }
    }

    return audiAndBmwCars;
}

module.exports = problem6;
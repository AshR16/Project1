function problem3(inventory) {
    return inventory.sort((a, b) => {
        let firstName = a.car_model.toUpperCase();
        let secondName = b.car_model.toUpperCase();
        if(firstName < secondName){
            return -1; //firstname placed before second
        }else if(firstName > secondName) {
            return 1; //secondname placed after firstname
        }
        return 0;//both are equal
      });
    
}

module.exports = problem3;